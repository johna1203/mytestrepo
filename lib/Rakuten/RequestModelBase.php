<?php
require_once __DIR__.'/ModelBase.php';

class Rakuten_RequestModelBase extends Rakuten_ModelBase
{
	protected $baseElementName = 'RequestModel';
}
