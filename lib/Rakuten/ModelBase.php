<?php
class Rakuten_ModelBase
{
	protected $baseElementName = 'Model';
	protected $elementName = '';
	protected $arrMembers = array();

	protected function getElementName() {
		return $this->elementName.$this->baseElementName;
	}

	public function setParams($params) {
		foreach($this->arrMembers as $name=>$attr) {
			if(array_key_exists($name, $params)) {
				if($attr[0] == Rakuten::RAKUTEN_MODEL_MODEL_ELEMENT) {
					$class = $this->arrMembers[$name][3];
					$subModel = new $class();
					$subModel->setParams($params[$name]);
					$this->arrMembers[$name][2]= $subModel;
				}elseif($attr[0] == Rakuten::RAKUTEN_MODEL_MODEL_ARRAY_ELEMENT) {
					foreach($params[$name] as $data) {
						$class = $this->arrMembers[$name][3];
						$subModel = new $class();
						$subModel->setParams($data);
						$this->arrMembers[$name][2][]= $subModel;
					}
				}else{
					$this->arrMembers[$name][2] = $params[$name];
				}
			}
		}
	}

	public function toXml($dom, $root) {
		$modelRoot = $root->appendChild($dom->createElement($this->getElementName()));
		foreach($this->arrMembers as $key=>$info) {
			if($info[0] == Rakuten::RAKUTEN_MODEL_MODEL_ELEMENT) {
                if(!empty($info[2])) {
				    $info[2]->toXml($dom, $modelRoot);
                }
			}elseif($info[0] == Rakuten::RAKUTEN_MODEL_ARRAY_ELEMENT) {
				foreach($info[2] as $value) {
					$child = $modelRoot->appendChild($dom->createElement($info[1], $value));
				}
			}elseif($info[0] == Rakuten::RAKUTEN_MODEL_MODEL_ARRAY_ELEMENT) {
				foreach($info[2] as $data) {
					$data->toXml($dom, $modelRoot);
				}
			}else{
				$child = $modelRoot->appendChild($dom->createElement($info[1], $info[2]));
			}
		}
	}
}
