<?php
require_once __DIR__.'/../ApiBase.php';
require_once __DIR__.'/model/request/GetOrder.php';
require_once __DIR__.'/model/response/GetOrder.php';

class Rakuten_Order_GetOrder extends Rakuten_ApiBase
{
	protected $functionName = 'GetOrders';
	protected $sendXmlUrl = '/rakuten/order/requestApi';
	
	public function __construct() {
		$this->model = new Rakuten_Order_Model_Request_GetOrder();
		$this->responseModel = new Rakuten_Order_Model_Response_GetOrder();
	}
	
}
