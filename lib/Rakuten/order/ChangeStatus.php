<?php
require_once __DIR__.'/../ApiBase.php';
require_once __DIR__.'/model/request/ChangeStatus.php';
require_once __DIR__.'/model/response/ChangeStatus.php';

class Rakuten_Order_ChangeStatus extends Rakuten_ApiBase
{
	protected $functionName = 'ChangeStatus';
	protected $sendXmlUrl = '/rakuten/order/requestApi';
	
	public function __construct() {
		$this->model = new Rakuten_Order_Model_Request_ChangeStatus();
		$this->responseModel = new Rakuten_Order_Model_Response_ChangeStatus();
	}

}
