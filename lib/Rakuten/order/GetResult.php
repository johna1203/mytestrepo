<?php
require_once __DIR__.'/../ApiBase.php';
require_once __DIR__.'/model/request/GetResult.php';
require_once __DIR__.'/model/response/GetResult.php';

class Rakuten_Order_GetResult extends Rakuten_ApiBase
{
	protected $functionName = 'GetResult';
	protected $sendXmlUrl = '/rakuten/order/requestApi';
	
	public function __construct() {
		$this->model = new Rakuten_Order_Model_Request_GetResult();
		$this->responseModel = new Rakuten_Order_Model_Response_GetResult();
	}
	
}
