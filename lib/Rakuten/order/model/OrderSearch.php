<?php
require_once __DIR__.'/../../ModelBase.php';

class Rakuten_Order_Model_OrderSearch extends Rakuten_ModelBase
{
	protected $elementName = 'orderSearch';
	protected $arrMembers = array(
			'dateType' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'dateType', ''),
			'startDate' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'startDate', ''),
			'endDate' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'endDate', ''),
	);

}