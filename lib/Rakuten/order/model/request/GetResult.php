<?php
require_once __DIR__.'/../../../RequestModelBase.php';

class Rakuten_Order_Model_Request_GetResult extends Rakuten_RequestModelBase
{
	protected $elementName = 'getResult';
	
	protected $arrMembers = array(
			'requestIds' => array(Rakuten::RAKUTEN_MODEL_ARRAY_ELEMENT, 'requestId', array(), ''),
	);

}
