<?php
require_once __DIR__.'/../../../RequestModelBase.php';
require_once __DIR__.'/../OrderStatus.php';

class Rakuten_Order_Model_Request_ChangeStatus extends Rakuten_RequestModelBase
{
	protected $elementName = 'changeStatus';

	protected $arrMembers = array(
			'orderStatus' => array(Rakuten::RAKUTEN_MODEL_MODEL_ARRAY_ELEMENT, 'orderStatus', array(), 'Rakuten_Order_Model_OrderStatus'),
	);

}
