<?php
require_once __DIR__.'/../../../RequestModelBase.php';
require_once __DIR__.'/../OrderSearch.php';

class Rakuten_Order_Model_Request_GetOrder extends Rakuten_RequestModelBase
{
	protected $elementName = 'getOrder';
	
	protected $arrMembers = array(
			'isOrderNumberOnly' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'isOrderNumberOnlyFlg', 'false', ''),
			'orderNo' => array(Rakuten::RAKUTEN_MODEL_ARRAY_ELEMENT, 'orderNumber', array(), ''),
			'search' => array(Rakuten::RAKUTEN_MODEL_MODEL_ELEMENT, 'orderSearch', NULL, 'Rakuten_Order_Model_OrderSearch'),
	);

}
