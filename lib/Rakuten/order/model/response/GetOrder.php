<?php
require_once __DIR__.'/../../../ResponseModelBase.php';

class Rakuten_Order_Model_Response_GetOrder extends Rakuten_ResponseModelBase
{
	public function getResponseBody() {
		return $this->responseData->order;
	}
}
