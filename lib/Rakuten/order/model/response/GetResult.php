<?php
require_once __DIR__.'/../../../ResponseModelBase.php';

class Rakuten_Order_Model_Response_GetResult extends Rakuten_ResponseModelBase
{
	public function getResponseBody() {
		return $this->responseData->asyncResult;
	}
}
