<?php
require_once __DIR__.'/../../ModelBase.php';

class Rakuten_Order_Model_OrderStatus extends Rakuten_ModelBase
{
	protected $elementName = 'orderStatus';
	protected $arrMembers = array(
			'orderNums' => array(Rakuten::RAKUTEN_MODEL_ARRAY_ELEMENT, 'orderNumber', array(), ''),
			'status' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'statusName', ''),
	);

}