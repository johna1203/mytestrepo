<?php
require_once __DIR__.'/ModelBase.php';

class Rakuten_ResponseModelBase extends Rakuten_ModelBase
{
	protected $responseData;
	protected $plainXml = '';
	protected $strXml = '';

	public function setResult($result) {
		$this->setResponseData(simplexml_load_string($this->replaceXmlElements($result)));
	}
	
    protected function replaceXmlElements($result) {
    	$this->plainXml = $result;
    	$this->strXml = mb_ereg_replace('<(\/*[a-zA-Z0-9]+)Model.*?>', '<\\1>', $result);
    	$this->strXml = mb_ereg_replace('<(\/*)[a-zA-Z0-9]+:([a-zA-Z0-9]+)Model.*?>', '<\\1\\2>', $this->strXml);
    	return $this->strXml;
    }
	
	protected function setResponseData($data) {
		$this->responseData = $data;
	}
	
	protected function getResponseData() {
		return $this->responseData;
	}

	public function getResponseBody() {
		return $this->responseData;
	}
	
	public function getErrorCode() {
		return $this->responseData->errorCode;
	}

	public function getErrorMessage() {
		return $this->responseData->message;
	}
}
