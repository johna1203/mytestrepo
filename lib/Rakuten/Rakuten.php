<?php
require_once __DIR__.'/UserAuth.php';
require_once __DIR__.'/order/GetResult.php';
require_once __DIR__.'/order/GetOrder.php';
require_once __DIR__.'/order/ChangeStatus.php';
require_once __DIR__.'/payment/DoAuthori.php';
require_once __DIR__.'/payment/DoSales.php';
require_once __DIR__.'/payment/GetResult.php';

class Rakuten
{
	const RAKUTEN_API_SERVER_HOST = 'cherry.ec-giken.com';
	const RAKUTEN_API_SERVER_PROTOCOL = 'http://';
	const RAKUTEN_API_SERVER_PORT = '8080';
	
	const RAKUTEN_MODEL_ONE_ELEMENT = 1;
	const RAKUTEN_MODEL_ARRAY_ELEMENT = 2;
	const RAKUTEN_MODEL_MODEL_ELEMENT = 3;
	const RAKUTEN_MODEL_MODEL_ARRAY_ELEMENT = 4;

	const PAYMENT_TYPE_ONE = '0';
	const PAYMENT_TYPE_REVO = '2';
	const PAYMENT_TYPE_BONUS = '4';
	const PAYMENT_TYPE_3 = '103';
	const PAYMENT_TYPE_5 = '105';
	const PAYMENT_TYPE_6 = '106';
	const PAYMENT_TYPE_10 = '110';
	const PAYMENT_TYPE_12 = '112';
	const PAYMENT_TYPE_15 = '115';
	const PAYMENT_TYPE_18 = '118';
	const PAYMENT_TYPE_20 = '120';
	const PAYMENT_TYPE_24 = '124';
}
