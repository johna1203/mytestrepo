<?php
require_once __DIR__.'/../ApiBase.php';
require_once __DIR__.'/model/request/GetResult.php';
require_once __DIR__.'/model/response/GetResult.php';

class Rakuten_Payment_GetResult extends Rakuten_ApiBase
{
	protected $functionName = 'getRCCSResult';
	protected $sendXmlUrl = '/rakuten/rccs/requestApi';
	
	public function __construct() {
		$this->model = new Rakuten_Payment_Model_Request_GetResult();
		$this->responseModel = new Rakuten_Payment_Model_Response_GetResult();
	}
	
}
