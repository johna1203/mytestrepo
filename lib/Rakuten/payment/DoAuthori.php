<?php
require_once __DIR__.'/../ApiBase.php';
require_once __DIR__.'/model/request/DoAuthori.php';
require_once __DIR__.'/model/response/DoAuthori.php';

class Rakuten_Payment_DoAuthori extends Rakuten_ApiBase
{
	protected $functionName = 'authori';
	protected $sendXmlUrl = '/rakuten/rccs/requestApi';
	
	public function __construct() {
		$this->model = new Rakuten_Payment_Model_Request_DoAuthori();
		$this->responseModel = new Rakuten_Payment_Model_Response_DoAuthori();
	}
	
}
