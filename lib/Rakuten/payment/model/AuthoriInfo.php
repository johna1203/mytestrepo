<?php
require_once __DIR__.'/../../ModelBase.php';

class Rakuten_Payment_Model_AuthoriInfo extends Rakuten_ModelBase
{
	protected $elementName = 'order';
	protected $arrMembers = array(
			'orderNo' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'orderNumber', ''),
			'price' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'price', ''),
			'payType' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'payType', ''),
	);

}