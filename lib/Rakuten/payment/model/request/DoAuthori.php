<?php
require_once __DIR__.'/../../../RequestModelBase.php';
require_once __DIR__.'/../AuthoriInfo.php';

class Rakuten_Payment_Model_Request_DoAuthori extends Rakuten_RequestModelBase
{
	protected $elementName = 'authori';
	
	protected $arrMembers = array(
			'orders' => array(Rakuten::RAKUTEN_MODEL_MODEL_ARRAY_ELEMENT, 'uiAuthori', NULL, 'Rakuten_Payment_Model_AuthoriInfo'),
	);

}
