<?php
require_once __DIR__.'/../../../RequestModelBase.php';
require_once __DIR__.'/../SalesInfo.php';

class Rakuten_Payment_Model_Request_DoSales extends Rakuten_RequestModelBase
{
	protected $elementName = 'sales';
	
	protected $arrMembers = array(
			'orders' => array(Rakuten::RAKUTEN_MODEL_MODEL_ARRAY_ELEMENT, 'sales', NULL, 'Rakuten_Payment_Model_SalesInfo'),
	);

}
