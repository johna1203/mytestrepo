<?php
require_once __DIR__.'/../../ModelBase.php';

class Rakuten_Payment_Model_SalesInfo extends Rakuten_ModelBase
{
	protected $elementName = 'sales';
	protected $arrMembers = array(
			'orderNo' => array(Rakuten::RAKUTEN_MODEL_ONE_ELEMENT, 'orderNumber', ''),
	);

}