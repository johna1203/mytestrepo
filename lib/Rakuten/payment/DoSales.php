<?php
require_once __DIR__.'/../ApiBase.php';
require_once __DIR__.'/model/request/DoSales.php';
require_once __DIR__.'/model/response/DoSales.php';

class Rakuten_Payment_DoSales extends Rakuten_ApiBase
{
	protected $functionName = 'sales';
	protected $sendXmlUrl = '/rakuten/rccs/requestApi';
	
	public function __construct() {
		$this->model = new Rakuten_Payment_Model_Request_DoSales();
		$this->responseModel = new Rakuten_Payment_Model_Response_DoSales();
	}
	
}
