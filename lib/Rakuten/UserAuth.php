<?php
class Rakuten_UserAuth
{
	const ELEMENT_NAME_AUTHKEY = 'authKey';
	const ELEMENT_NAME_SHOPURL = 'shopUrl';
	const ELEMENT_NAME_USERNAME = 'userName';
	protected $_elementName = 'userAuthModel';
	
	public $authKey = "";
	public $shopUrl = "";
	public $userName = "";
	
	public function elementName() {
		return $this->_elementName;
	}
	
	public function toXml($dom, $root) {
		$authKey = $root->appendChild($dom->createElement(self::ELEMENT_NAME_AUTHKEY, $this->authKey));
		$shopUrl = $root->appendChild($dom->createElement(self::ELEMENT_NAME_SHOPURL, $this->shopUrl));
		$userName = $root->appendChild($dom->createElement(self::ELEMENT_NAME_USERNAME, $this->userName));
	}
}