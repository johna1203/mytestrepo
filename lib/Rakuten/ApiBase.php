<?php
class Rakuten_ApiBase
{
	const XML_ROOT_NAME = 'requestModel';
	const ELEMENT_NAME_FUNCTION = 'function';
	protected $functionName;
	public $model;
	public $responseModel;
	public $userAuth = NULL;
	protected $sendXmlUrl = '';
	
	public function request() {
		$ch = curl_init(Rakuten::RAKUTEN_API_SERVER_PROTOCOL.Rakuten::RAKUTEN_API_SERVER_HOST.':'.Rakuten::RAKUTEN_API_SERVER_PORT.$this->sendXmlUrl);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->toXml());
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$ch_result = curl_exec($ch);
		if($ch_result) {
//			echo htmlspecialchars($ch_result);
			$this->responseModel->setResult($ch_result);
		}else{
			return FALSE;
		}
		curl_close($ch);
		return TRUE;
	}
	
	public function setParams($params) {
		$this->model->setParams($params);
	}

	public function toXml() {
		$dom = new DOMDocument('1.0', 'UTF-8');
		$root = $dom->appendChild($dom->createElement(self::XML_ROOT_NAME));
		$function = $root->appendChild($dom->createElement(self::ELEMENT_NAME_FUNCTION, $this->functionName));
		if(!empty($this->userAuth)) {
	        $auth = $root->appendChild($dom->createElement($this->userAuth->elementName()));
	        $this->userAuth->toXml($dom, $auth);
		}
        $this->model->toXml($dom, $root);
		
		$dom->formatOutput = TRUE;
		return $dom->saveXML();
	}
}
