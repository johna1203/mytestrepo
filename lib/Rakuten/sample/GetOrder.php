<?php
require_once '../Rakuten.php';

/**
 * 受注詳細取得サンプルです
 */

//認証情報です。店舗毎に必要な情報です。
$auth = new Rakuten_UserAuth();
$auth->authKey = '1895f9af05b35acae71b6644892f73e7';
$auth->shopUrl = '_partner_2382';
$auth->userName = 'partner2382';

//受注詳細取得オブジェクトを作ります。
$model = new Rakuten_Order_GetOrder();
//パラメータに受注番号を入れます。orderNoはarrayなので、一括して複数の情報取得も可
$model->setParams(array('orderNo'=>array('284858-20130527-0939770215')));
$model->userAuth = $auth;
//echo $model->toXml();   //リクエストXMLの内容をデバッグ表示
if($model->request()) {
    echo "\nError Code: ".$model->responseModel->getErrorCode()."\n";
    if($model->responseModel->getErrorCode() == 'N00-000') {    //エラーコード　N00-000　は正常（エラーコードは近い将来変わる可能性が…）
        $res = $model->responseModel->getResponseBody();   //レスポンスボディ（受注詳細）がオブジェクトで返ります。
        var_dump($res);
    }else{
        echo $model->responseModel->getMessage();   //エラーメッセージ
    }
}else{
    echo "Rakuten Request Error";
}

